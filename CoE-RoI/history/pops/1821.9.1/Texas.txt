132 = {
	capitalists = {

		size = 100
		culture = dixie
		religion = protestant

	}
	artisans = {

		size = 30
		culture = dixie
		religion = protestant

	}
	bureaucrats = {

		size = 9
		culture = dixie
		religion = protestant

	}
	clergymen = {

		size = 40
		culture = dixie
		religion = protestant

	}
	labourers = {

		size = 900
		culture = dixie
		religion = protestant

	}
	officers = {

		size = 98
		culture = dixie
		religion = protestant

	}
	soldiers = {

		size = 700
		culture = dixie
		religion = protestant

	}
	labourers = {

		size = 340
		culture = native_american_minor
		religion = catholic

	}
	labourers = {

		size = 450
		culture = mexican
		religion = catholic

	}
	labourers = {

		size = 200
		culture = cherokee
		religion = animist

	}
	slaves = {

		size = 100
		culture = afro_american
		religion = protestant

	}

}
133 = {

	labourers = {

		size = 1200
		culture = native_american_minor
		religion = animist

	}
	labourers = {

		size = 1000
		culture = mexican
		religion = catholic

	}
	artisans = {

		size = 250
		culture = dixie
		religion = protestant

	}
	clergymen = {

		size = 50
		culture = dixie
		religion = protestant

	}
	labourers = {

		size = 300
		culture = dixie
		religion = protestant

	}
	slaves = {

		size = 650
		culture = afro_american
		religion = protestant

	}

}
134 = {

	labourers = {

		size = 1200
		culture = native_american_minor
		religion = animist

	}
	artisans = {

		size = 300
		culture = dixie
		religion = protestant

	}
	clergymen = {

		size = 60
		culture = dixie
		religion = protestant

	}
	bureaucrats = {

		size = 60
		culture = dixie
		religion = protestant

	}
	labourers = {

		size = 500
		culture = dixie
		religion = protestant

	}
	slaves = {

		size = 80
		culture = afro_american
		religion = protestant

	}

}
135 = {

	capitalists = {

		size = 100
		culture = native_american_minor
		religion = animist

	}
	capitalists = {

		size = 50
		culture = mexican
		religion = catholic

	}
	artisans = {

		size = 98
		culture = native_american_minor
		religion = animist

	}
	clergymen = {

		size = 49
		culture = native_american_minor
		religion = animist

	}
	labourers = {

		size = 1781
		culture = native_american_minor
		religion = animist

	}
	soldiers = {

		size = 64
		culture = native_american_minor
		religion = animist

	}
	soldiers = {

		size = 80
		culture = mexican
		religion = catholic

	}
	artisans = {

		size = 106
		culture = dixie
		religion = protestant

	}
	bureaucrats = {

		size = 40
		culture = dixie
		religion = protestant

	}
	clergymen = {

		size = 25
		culture = dixie
		religion = protestant

	}
	labourers = {

		size = 1291
		culture = dixie
		religion = protestant

	}

}
137 = {

	labourers = {

		size = 1200
		culture = native_american_minor
		religion = animist

	}
	labourers = {

		size = 1100
		culture = mexican
		religion = catholic

	}
	artisans = {

		size = 90
		culture = dixie
		religion = protestant

	}
	bureaucrats = {

		size = 60
		culture = dixie
		religion = protestant

	}
	labourers = {

		size = 800
		culture = dixie
		religion = protestant

	}
	slaves = {

		size = 700
		culture = afro_american
		religion = protestant

	}

}