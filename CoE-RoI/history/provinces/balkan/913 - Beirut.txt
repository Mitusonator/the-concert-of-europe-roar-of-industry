owner = TUR
controller = TUR
add_core = TUR
add_core = LBN
trade_goods = fish
life_rating = 30
terrain = dry_forest_mountains
colonial = 1
1836.1.1 = {
	owner = EGY
	controller = EGY
	colonial = 0
}
1861.1.1 = {
	controller = TUR 
	owner = TUR
	state_building = {
		level = 1
		building = heavy_factory_building
		upgrade = yes
	}
}