owner = PRU
controller = PRU
add_core = PRU
add_core = SLS
add_core = GER
trade_goods = coal
life_rating = 30
terrain = deciduous_forest_hills
state_building = {
	level = 1
	building = heavy_factory_building
	upgrade = yes
}
1836.1.1 = {
	state_building = {
		level = 1
		building = heavy_factory_building
		upgrade = yes
	}
	state_building = {
		level = 2
		building = heavy_factory_building
		upgrade = yes
	}
}
1861.1.1 = {
	state_building = {
		level = 1
		building = heavy_factory_building
		upgrade = yes
	}
	state_building = {
		level = 2
		building = heavy_factory_building
		upgrade = yes
	}
	state_building = {
		level = 1
		building = heavy_factory_building
		upgrade = yes
	}
	state_building = {
		level = 2
		building = heavy_factory_building
		upgrade = yes
	}
}
1861.1.1 = { railroad = 3 }
